# A number guessing GAME

A small app which is not the best Game in the world, basicly we just need to guess a number between a defined certain range "1 to 10" and after 3 wrong attempts the game is over and you can't add any more values unless you start playing again which is very important for making it clear and what the user suppose to do.

## GAME GUIDE:
* Player must guess a number between a min and max
* Player gets 3 attempts before Game Over
* Notify The player of attempts remaining
* Notify the player of the correct answer if loose
* Let the player to choose to play again

### Prerequisites

None

```
Enjoy the game and try your luck
```

### Installing

git clone https://github.com/letowebdev/Number-Guessing-Game.git


## Deployment

https://zacheleto.me/Projects/gameguesser

## Built With

* [Plain JavaScript]
* [Skeleton CDN]


## Author

* **Zache Abdelatif (Leto)** 

## License

This project is licensed under the MIT License
