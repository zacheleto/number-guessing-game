// Game values
let min = 1,
    max = 10,
    winningNumber = getRandomNum(min, max),
    guessesLeft = 3;

// Defining the UI elements
const UIgame = document.querySelector('#game'),
      UIminNum = document.querySelector('.min-num'),
      UImaxNum = document.querySelector('.max-num'),
      UIguessBtn = document.querySelector('#guess-btn'),
      UIguessInput = document.querySelector('#guess-input'),
      UImessage = document.querySelector('.message');

// assign UI min and max
UIminNum.textContent = min;
UImaxNum.textContent = max;

// Play again event Listner
UIgame.addEventListener('mousedown', function(e){
    if (e.target.className === 'play-again') {
        window.location.reload();
    }
})

// Guess event listner
UIguessBtn.addEventListener('click', function(){
    let guess = parseInt(UIguessInput.value);
    console.log(guess)

    
    // Game over - WON
    if (guess === winningNumber) {
        gameOver(true, `${winningNumber} is correct, YOU WIN!`)
    } else {
     // Wrong number
     guessesLeft -= 1;
     if (guessesLeft === 0) {
         // Game over - LOST
            gameOver(false, `GAME OVER, you lost. The correct number was ${winningNumber}`)
     }else{
         // Game continues - Ansyer wrong

         // Change border color
         UIguessInput.style.borderColor = 'red'

         // Clearing the input
         UIguessInput.value = '';

         // Telling user that's the wrong number
         UIguessInput.style.borderColor = 'red'
         setMessage(`${guess} is not correct, ${guessesLeft} guesses left`, 'red')
         
     }  
    }

    // Validate
    if (isNaN(guess) || guess < min || guess > max) {
        setMessage(`Please enter a number between ${min} and ${max}`, 'red');
    } 
});

// Game Over to prevent repeating
function gameOver(won, msg)
{
        let color;
        won === true ? color = 'green' : color = 'red'; 
        // Disable input
        UIguessInput.disabled = true;
        // Change border color
        UIguessInput.style.borderColor = color
        // set message
        setMessage(msg, color)

        //Play again
        UIguessBtn.value = 'Play Again';
        UIguessBtn.className += 'play-again'
}

// Get Wining Number
function getRandomNum(min, max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}

// set Message
function setMessage(msg, color)
{
    UImessage.textContent = msg;
    UImessage.style.color = color;
}



      
